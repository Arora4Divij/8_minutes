var mongoose = require('mongoose'),
    util = require('../util'),
    Schema = mongoose.Schema;

var DynamicValuesSchema = new Schema({
	assumptions : {
		system_size : util.dbValidation.numReq,
		onm_escalation : util.dbValidation.numReq
	},
	utility : {
		appc : util.dbValidation.numReq,
		escalation : util.dbValidation.numReq,
		consumption_growth : util.dbValidation.numReq
	},

	pp_std : {
		escalation : util.dbValidation.numReq,
		term : util.dbValidation.numReq,
		genration_deg : util.dbValidation.numReq
	},

	pp_fifteen : {
		escalation : util.dbValidation.numReq,
		term : util.dbValidation.numReq,
		genration_deg : util.dbValidation.numReq
	},

	pp_thirty : {
		escalation : util.dbValidation.numReq,
		term : util.dbValidation.numReq,
		genration_deg : util.dbValidation.numReq
	},

	power : {
		interest_rate : util.dbValidation.numReq,
		term : util.dbValidation.numReq,
		upfront_cost : util.dbValidation.numReq
	},

	state : [{
		name : util.dbValidation.strReqUniq,
		generation_sum : util.dbValidation.numReq,
		current_tariff : util.dbValidation.numReq,
		distribution : {
			jan : util.dbValidation.numReq,
			feb : util.dbValidation.numReq,
			mar : util.dbValidation.numReq,
			apr : util.dbValidation.numReq,
			may : util.dbValidation.numReq,
			jun : util.dbValidation.numReq,
			jul : util.dbValidation.numReq,
			aug : util.dbValidation.numReq,
			sep : util.dbValidation.numReq,
			oct : util.dbValidation.numReq,
			nov : util.dbValidation.numReq,
			dec : util.dbValidation.numReq
		}
	}]

});	

module.exports = mongoose.model('dynamic_values', DynamicValuesSchema);


