var mongoose         = require('mongoose'),
    Schema           = mongoose.Schema,
    util             = require('../util');


var subAdminMapSchema =  new Schema({
	username : util.dbValidation.strReqUniq,
	designation : util.dbValidation.strReq
});

subAdminMapSchema.options.toJSON = {
    transform: function(doc, ret, options) {
        ret.id = ret._id;
        delete ret._id;
        delete ret.__v;
        return ret;
    }
};

module.exports = mongoose.model('sub-admin-map',subAdminMapSchema);