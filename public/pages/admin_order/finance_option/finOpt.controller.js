angular.module('eightMinutes')
	.controller('adminOrderFinOptController',["$scope","$state",function($scope,$state){
		

		var id = $state.params.id;

		function getAssesmentSuccess(d) {
		
		var selectedPlanColumIndex = 0;
		switch(d.plan_chosen){
			case "fin_upfront":{
				selectedPlanColumIndex = 1;
				break;	
			}
			case "fin_paypartial_plan1":{

				selectedPlanColumIndex = 2;
				break;	
			}
			case "fin_paypartial_plan2":{

				selectedPlanColumIndex = 3;
				break;	
			}
			case "paynothing_plan":{

				selectedPlanColumIndex = 4;
				break;	
			}
		}

		if(selectedPlanColumIndex){
			var $style = $("<style>#financeOpt tr td:nth-child("+ (selectedPlanColumIndex+1) +"),#financeOpt tr th:nth-child("+ (selectedPlanColumIndex+1) +"){background-color:#ffff99;}</style>");
			$("body").append($style);
		}

		if(d.is_ctr_status){

			// any data available

		}else{
			// no data available
			//$("#editView1").attr("checked","checked");
		}

		
		

				


		//$("#fin_upfront").val(d.fin_upfront);                     
		$("#fin_upfront_cost").val(d.fin_upfront_cost);                 
		$("#fin_upfront_aft_inc").val(d.fin_upfront_aft_inc);             
		$("#fin_upfront_anual_save").val(d.fin_upfront_anual_save);          
		$("#fin_upfront_yr20_save").val(d.fin_upfront_yr20_save);     
		$("#fin_upfront_advance_amount").val(d.fin_upfront_advance_amount || 10000);
		$("#fin_upfront_security_deposit").val(d.fin_upfront_security_deposit);      

		//$("#fin_paypartial_plan1").val(d.fin_paypartial_plan1);            
		$("#fin_paypartial_plan1_cost").val(d.fin_paypartial_plan1_cost);       
		$("#fin_paypartial_plan1_aft_inc").val(d.fin_paypartial_plan1_aft_inc);    
		$("#fin_paypartial_plan1_anual_save").val(d.fin_paypartial_plan1_anual_save); 
		$("#fin_paypartial_plan1_yr20_save").val(d.fin_paypartial_plan1_yr20_save); 
		$("#fin_paypartial_plan1_advance_amount").val(d.fin_paypartial_plan1_advance_amount || 10000);
		$("#fin_paypartial_plan1_security_deposit").val(d.fin_paypartial_plan1_security_deposit); 

		//$("#fin_paypartial_plan2").val(d.fin_paypartial_plan2);            
		$("#fin_paypartial_plan2_cost").val(d.fin_paypartial_plan2_cost);       
		$("#fin_paypartial_plan2_aft_inc").val(d.fin_paypartial_plan2_aft_inc);    
		$("#fin_paypartial_plan2_anual_save").val(d.fin_paypartial_plan2_anual_save); 
		$("#fin_paypartial_plan2_yr20_save").val(d.fin_paypartial_plan2_yr20_save); 
		$("#fin_paypartial_plan2_advance_amount").val(d.fin_paypartial_plan2_advance_amount || 10000);
		$("#fin_paypartial_plan2_security_deposit").val(d.fin_paypartial_plan2_security_deposit); 

		//$("#paynothing_plan").val(d.paynothing_plan);                
		$("#paynothing_plan_cost").val(d.paynothing_plan_cost);           
		$("#paynothing_plan_aft_inc").val(d.paynothing_plan_aft_inc);        
		$("#paynothing_plan_anual_save").val(d.paynothing_plan_anual_save);     
		$("#paynothing_plan_yr20_save").val(d.paynothing_plan_yr20_save);
		$("#paynothing_plan_advance_amount").val(d.paynothing_plan_advance_amount || 10000);
		$("#paynothing_plan_security_deposit").val(d.paynothing_plan_security_deposit);
	
		$("#file1").prev().attr("src",d.fin_upfront_ctr_copy && d.fin_upfront_ctr_copy.file);           
		$("#file1").prev().attr("alt",d.fin_upfront_ctr_copy && d.fin_upfront_ctr_copy.ext);           
		$("#file3").prev().attr("src",d.fin_paypartial_plan2_ctr_copy && d.fin_paypartial_plan2_ctr_copy.file);  
		$("#file3").prev().attr("alt",d.fin_paypartial_plan2_ctr_copy && d.fin_paypartial_plan2_ctr_copy.ext);  
		$("#file2").prev().attr("src",d.fin_paypartial_plan1_ctr_copy && d.fin_paypartial_plan1_ctr_copy.file);  
		$("#file2").prev().attr("alt",d.fin_paypartial_plan1_ctr_copy && d.fin_paypartial_plan1_ctr_copy.ext);  
		$("#file4").prev().attr("src",d.paynothing_plan_ctr_copy && d.paynothing_plan_ctr_copy.file);      
		$("#file4").prev().attr("alt",d.paynothing_plan_ctr_copy && d.paynothing_plan_ctr_copy.ext);   

		$("#warranty").html(d.warranty || "Not selected");   
	}




	$('[type="file"]').change(function(e){
	    if (this.files && this.files[0]) {
	        var reader = new FileReader();

	        var self = this;
	        reader.onload = function (e) {
	        	
	            $(self).prev().attr('src', e.target.result);
	            $(self).prev().attr('alt', $(self).val().split(".").pop() );
	        }

	        reader.readAsDataURL(this.files[0]);
	    }
	});

	$.ajax({
		url:'/api/assessment/'+id,
		type:"POST",
		data:{token : window.sessionStorage.token},
        error: function () {
	        
			document.location.href = "admin_order-list.html"

        },
		success: function (d) {
			getAssesmentSuccess(d);
			if(!d.plan_chosen){
				$("#check-status").show()
			}
		}
	});


	$.ajax({
		url:'/api/requirement/'+id,
		type:"POST",
		data:{token : window.sessionStorage.token},
        error: function () {
	        
			document.location.href = "admin_order-list.html"

        },
		success: function (d) {
			d.status = d.status || "pending";
			if(d.status == "pending"){
				$("#upload-contract").show()
			} 

			if(d.status === "View_Contract_Summary"){
					$("#upload-design").show()
			}

			

			$("#UserDetails").html(d.owner_salutation + ". "+ d.owner_fname+ " "+ d.owner_lname + " - " + d.location)
			$("#orderNo").html(d.orderid);
		}
	});


		

	$("#upload-contract").on("click",function () {
		$.ajax({
			url:'/api/assessment/'+id,
			type:"PATCH",
			data:{ token : window.sessionStorage.token},
            error: function () {
            },
			success: function (d) {
				showNotification("<h4>Success! Contract information was uploaded and is visiable to customer.</h4>", "success")	
				getAssesmentSuccess(d)
			}
		});

		$.ajax({
			url:'/api/requirement/'+id,
			type:"PATCH",
			data:{ "status" : "View_Proposal" ,token : window.sessionStorage.token},
            error: function () {
            },
			success: function (d) {
			}
		});


	});

	$("#upload-design").on("click",function () {

		var r = confirm("All payments have been received!");
		if (r == true) {
			document.location.href = "admin_order-designs.html#"+id;
		}


	});
	$("#submit").on("click",function () {

		var data = {
			_id                             : id,
			fin_upfront                     : 100,//$("#fin_upfront").val(),                     
			fin_upfront_cost                : $("#fin_upfront_cost").val(),                 
			fin_upfront_aft_inc             : $("#fin_upfront_aft_inc").val(),             
			fin_upfront_anual_save          : $("#fin_upfront_anual_save").val(),          
			fin_upfront_yr20_save           : $("#fin_upfront_yr20_save").val(),           
			fin_upfront_advance_amount      : $("#fin_upfront_advance_amount").val(),           
			fin_upfront_security_deposit    : $("#fin_upfront_security_deposit").val(),           
			fin_upfront_ctr_copy            : {
												file:$("#file1").prev().attr("src"),
												ext:$("#file1").prev().attr("alt")

											   },           
			
			fin_paypartial_plan1                  : 15, //$("#fin_paypartial_plan1").val(),            
			fin_paypartial_plan1_cost             : $("#fin_paypartial_plan1_cost").val(),       
			fin_paypartial_plan1_aft_inc          : $("#fin_paypartial_plan1_aft_inc").val(),    
			fin_paypartial_plan1_anual_save       : $("#fin_paypartial_plan1_anual_save").val(), 
			fin_paypartial_plan1_yr20_save        : $("#fin_paypartial_plan1_yr20_save").val(),  
			fin_paypartial_plan1_advance_amount   : $("#fin_paypartial_plan1_advance_amount").val(),           
			fin_paypartial_plan1_security_deposit : $("#fin_paypartial_plan1_security_deposit").val(),           
			fin_paypartial_plan1_ctr_copy         : {
												file:$("#file2").prev().attr("src"),
												ext:$("#file2").prev().attr("alt")

											   },  
			
			fin_paypartial_plan2            : 30, //$("#fin_paypartial_plan2").val(),            
			fin_paypartial_plan2_cost       : $("#fin_paypartial_plan2_cost").val(),       
			fin_paypartial_plan2_aft_inc    : $("#fin_paypartial_plan2_aft_inc").val(),    
			fin_paypartial_plan2_anual_save : $("#fin_paypartial_plan2_anual_save").val(), 
			fin_paypartial_plan2_yr20_save  : $("#fin_paypartial_plan2_yr20_save").val(),  
			fin_paypartial_plan2_advance_amount      : $("#fin_paypartial_plan2_advance_amount").val(),           
			fin_paypartial_plan2_security_deposit    : $("#fin_paypartial_plan2_security_deposit").val(),           
			fin_paypartial_plan2_ctr_copy   : {
												file:$("#file3").prev().attr("src"),
												ext:$("#file3").prev().attr("alt")

											   },  
			
			paynothing_plan                 : 0, //$("#paynothing_plan").val(),                
			paynothing_plan_cost            : $("#paynothing_plan_cost").val(),           
			paynothing_plan_aft_inc         : $("#paynothing_plan_aft_inc").val(),        
			paynothing_plan_anual_save      : $("#paynothing_plan_anual_save").val(),     
			paynothing_plan_yr20_save       : $("#paynothing_plan_yr20_save").val(),
			paynothing_plan_advance_amount      : $("#paynothing_plan_advance_amount").val(),           
			paynothing_plan_security_deposit    : $("#paynothing_plan_security_deposit").val(),           
			paynothing_plan_ctr_copy        : {
												file:$("#file4").prev().attr("src"),
												ext:$("#file4").prev().attr("alt")

											   },
			token                           : window.sessionStorage.token


		};

		$.ajax({
			url:'/api/assessment/'+id,
			type:"PATCH",
			data:data,
            error: function () {
		        $.ajax({
					url:'/api/assessment',
					type:"POST",
					data:data,
		            error: function () {
				        document.location.href = "admin_order-list.html"
		            },
					success: function (d) {
						getAssesmentSuccess(d)
						$("#editView2").attr("checked","checked");
					}
				});

            },
			success: function (d) {
				getAssesmentSuccess(d);
				showNotification("<h4>Success! Contract was updated.</h4>", "success")	
				//document.location.href='admin_order-contract1.html#'+id;
			}
		});

	})


	$("#check-status").on("click",function(){
		$.ajax({
			url:'/api/assessment/'+id,
			type:"PATCH",
			data:{ /*"plan_chosen" : "fin_upfront" ,*/token : window.sessionStorage.token},
            error: function () {
				        document.location.href = "admin_order-list.html"
            },
			success: function (d) {
				if(!d.plan_chosen)
					showNotification("Success! User has not chosen any plan yet.", "success")	
				getAssesmentSuccess(d)
			}
		});
	})
}]);