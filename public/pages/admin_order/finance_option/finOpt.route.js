'use strict';

angular.module("eightMinutes")
  .config(["$stateProvider",function($stateProvider) {
  	$stateProvider
  		.state("admin.order.finOpt", {
  			url : "/finOpt?id",
  			templateUrl : "./pages/admin_order/finance_option/finOpt.html",
 			controller : "adminOrderFinOptController"
  		})
  }])
