angular.module('eightMinutes')
	.controller('adminOrderController',["$scope","$state",function($scope,$state){
		console.log("inside admin order");

        // $state.go('admin.order.details',{id:$state.params.id})

        $('#order-details').addClass('cj-active-tab');

		$("#order-details").on("click",function (e) {
			$state.go('admin.order.details',{id:$state.params.id})
            removeActiveTabClass('order-details');
            $('#order-details').addClass('cj-active-tab');
        });
        $("#installer-visit").on("click",function (e) {
      		$state.go('admin.order.installerVisit',{id:$state.params.id})
            removeActiveTabClass('order-details');
            $("#installer-visit").addClass('cj-active-tab');
        });
        $("#proposal").on("click",function (e) {
        	$state.go('admin.order.proposal',{id:$state.params.id})
            removeActiveTabClass('order-details');
            $("#proposal").addClass('cj-active-tab');
        });
        $("#fin-opt").on("click",function (e) {
        	$state.go('admin.order.finOpt',{id:$state.params.id})
            removeActiveTabClass('order-details');
            $("#fin-opt").addClass('cj-active-tab');
        });
        $("#design").on("click",function (e) {
        	$state.go('admin.order.designs',{id:$state.params.id})
            removeActiveTabClass('order-details');
            $("#design").addClass('cj-active-tab');
        });
        $("#installation").on("click",function (e) {
        	$state.go('admin.order.installation',{id:$state.params.id})
            removeActiveTabClass('order-details');
            $("#installation").addClass('cj-active-tab');
        });

        function removeActiveTabClass(){
            $('#order-details').removeClass('cj-active-tab');
            $('#installer-visit').removeClass('cj-active-tab');
            $('#proposal').removeClass('cj-active-tab');
            $('#fin-opt').removeClass('cj-active-tab');
            $('#design').removeClass('cj-active-tab');
            $('#installation').removeClass('cj-active-tab');
        }
}]);