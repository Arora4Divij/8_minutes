angular.module('eightMinutes')
	.controller('adminOrderDetailsController',["$scope","$state",function($scope,$state){
		$(function(){
		// var id = window.location.hash.split("");
		// id.shift();
		// id = id.join('');

		id = $state.params.id;

        var installer_id;

		$.ajax({
			url:"/api/requirement/"+id,
			type:"POST",
			data:{token:window.sessionStorage.token},
            error: function () {
                document.location.href = "user_dashboard.html"

            },
			success: function (d) {
				
				$("#owner-name").html(d.owner_salutation + " "+ d.owner_fname+ " "+ d.owner_lname)
				$("#owner-email").html(d.owner_email)
				$("#owner-phone").html(d.owner_phone)
				$("#installateion-address").html(d.address_l1 + "<br>" + d.address_l2 + "<br>" + d.location)
				$("#installateion-type").html(d.installAt)
				$("#roof-area").html(d.rooftop_area)
				$("#avg-bill").html(d.current_avg_bill)
				$("#req-date").html(dateFormat(d.req_visit_date))
				$("#req-time").html((d.req_visit_time))
				

				$("#UserDetails").html(d.owner_salutation + ". "+ d.owner_fname+ " "+ d.owner_lname + " - " + d.location)
				$("#orderNo").html(d.orderid)

				if(d.installer_id){
					$("#setupVisit").hide();
				}else{
					$("#next").hide();
				}

				$.ajax({
					url:"/api/user/un",
					type:"POST",
					data:{username:d.username,token:window.sessionStorage.token},
		            error: function () {
		                document.location.href = "admin_order-list.html"
		            },
					success: function (userdetails) {
						$("#user-full-name").html(userdetails.salutation + " "+ userdetails.fname+ " "+ userdetails.lname)
						$("#user-address").html(userdetails.address1 + "<br>" + userdetails.address2)
						$("#email").html(userdetails.email )
						$("#phone").html(userdetails.phone )
						
					}
				});
			}
		});



		$.ajax({
			url:"/api/company/all",
			type:"POST",
			data:{token:window.sessionStorage.token},
            error: function () {
		        document.location.href = "admin_order-list.html"
            },
			success: function (list) {
				$("#company").html("<option value=''>select</option>")
				for(var i in list){
					var item = list[i];
					var $elm = $("<option value=\""+ item._id + "\">"+ item.name +"</option>");
					$("#company").append($elm)
				}
				
			}
		});


		$("#company").on("change", function () {
			


			$.ajax({
				url:"/api/installer/all",
				type:"POST",
				data:{company_id:$(this).val(),token:window.sessionStorage.token},
	            error: function () {
			        document.location.href = "admin_order-list.html"
	            },
				success: function (list) {
					$("#installer").html("<option value=''>select</option>")
					for(var i in list){
						var item = list[i];
						var $elm = $("<option value=\""+ item._id + "\">"+ item.name +"</option>");
						$("#installer").append($elm)
					}
					
				}
			});

		})


		$("#assign").on("click", function () {
			var val = $("#installer").val();
			if(val)
			$.ajax({
				url:"/api/requirement/"+id,
				type:"PATCH",
				data:{installer_id:val,token:window.sessionStorage.token},
	            error: function () {
			        document.location.href = "admin_order-list.html"
	            },
				success: function (list) {
					document.location.href='admin_order-visit.html#'+id;
				}
			});

		})

		$("#next").on("click", function () {
			document.location.href='admin_order-visit.html#'+id;
		})


		$("#cancel").on("click", function () {
			document.location.href = "admin_order-list.html"
			
		})
	        

	})
}]);