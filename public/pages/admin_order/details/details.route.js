'use strict';

angular.module("eightMinutes")
  .config(["$stateProvider",function($stateProvider) {
  	$stateProvider
  		.state("admin.order.details", {
  			url : "/details?id",
  			templateUrl : "./pages/admin_order/details/details.html",
 			controller : "adminOrderDetailsController"
  		})
  }])
