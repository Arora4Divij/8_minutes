angular.module('eightMinutes')
	.controller('adminOrderInstallationController',["$scope","$state",function($scope,$state){
		
		var marker;
			    		var map;
			    		var locCord;
			    		var locDetails;
			    	  var isFirstTime = true;
					  function initAutocomplete() {

					  	if(!isFirstTime){
					  		return;
					  	}
			
						isFirstTime = false;

					    map = new google.maps.Map(document.getElementById('map'), {
					       zoom: 10,
				          //mapTypeId: 'satellite',
				          //mapTypeId: google.maps.MapTypeId.ROADMAP,
				          center: {lat : 28.5248825, lng : 77.190029}
					    });


					    // Create the search box and link it to the UI element.
					    var input = document.getElementById('pac-input');
					    var searchBox = new google.maps.places.SearchBox(input);
					    map.controls[google.maps.ControlPosition.TOP_LEFT].push(input);

					    // Bias the SearchBox results towards current map's viewport.
					    map.addListener('bounds_changed', function() {
					      searchBox.setBounds(map.getBounds());
					    });

					    var markers = [];
					    // Listen for the event fired when the user selects a prediction and retrieve
					    // more details for that place.
					    searchBox.addListener('places_changed', function() {
					      var places = searchBox.getPlaces();

					      if (places.length == 0) {
					        return;
					      }

					      // Clear out the old markers.
					      markers.forEach(function(marker) {
					        marker.setMap(null);
					      });
					      markers = [];

					      // For each place, get the icon, name and location.
					      var bounds = new google.maps.LatLngBounds();
					      places.forEach(function(place) {
					        if (!place.geometry) {
					          console.log("Returned place contains no geometry");
					          return;
					        }
					        var icon = {
					          url: place.icon,
					          size: new google.maps.Size(71, 71),
					          origin: new google.maps.Point(0, 0),
					          anchor: new google.maps.Point(17, 34),
					          scaledSize: new google.maps.Size(25, 25)
					        };

					        // Create a marker for each place.
					        markers.push(new google.maps.Marker({
					          map: map,
					          icon: icon,
					          title: place.name,
					          position: place.geometry.location
					        }));

					        if (place.geometry.viewport) {
					          // Only geocodes have viewport.
					          bounds.union(place.geometry.viewport);
					        } else {
					          bounds.extend(place.geometry.location);
					        }
					      });
					      map.fitBounds(bounds);
					    });

					    	if(locDetails){
						    	marker = new google.maps.Marker({
						          position: {lat : Number(locDetails.lat), lng : Number(locDetails.lng)},
						          map: map,
						          title: locDetails.installAt + "\n" + locDetails.dataOfInstallation
						        })
					    	}
					    	map.addListener('click', function(e) {
					
								if(marker){

									var latlng = new google.maps.LatLng(e.latLng.lat(), e.latLng.lng());
									marker.setPosition(latlng);
								}else{
									marker = new google.maps.Marker({
							          position: {lat : e.latLng.lat(), lng : e.latLng.lng()},
							          map: map
							        })
								}

								locCord = {lat:e.latLng.lat(),lng:e.latLng.lng()};

							  });
					  }


		$(function () {
			//Initialize Select2 Elements
			$(".select2").select2();
		});

		var id = $state.params.id;

	if(!id){
        document.location.href = "admin_order-list.html"
	}

    var installer_id,assessment,installation;

	$(function(){


		$.ajax({
			url:"/api/installation/"+id,
			type:"POST",
			data:{token:window.sessionStorage.token},
            error: function () {
                document.location.href = "admin_order-list.html"

            },
			success: function (d) {
				if(d && d.time_ln){
					for(var i in d.time_ln){
						$(".name").eq(i).html(d.time_ln[i].name);
						$(".desc").eq(i).val(d.time_ln[i].desc);
						$(".st_time").eq(i).val(d.time_ln[i].st_time);
						$(".ed_time").eq(i).val(d.time_ln[i].ed_time);

						var $sel = $(".status").eq(i);
						$sel.find("option").each(function () {
							$(this).removeAttr("selected")
							if($(this).text() == d.time_ln[i].status){
								$(this).attr("selected", "selected")							
							}
						})
					}
					$(".select2").select2();
					installation = d;
				}

			}
		});

		

		function toggleMap(d){
			if(d.status == "Installation_Completed"){
				$("#mapCnt").show();
				initAutocomplete();
			}else{
				$("#mapCnt").hide();
			}
		} 


		$.ajax({
			url:"/api/location/"+id,
			type:"POST",
			data:{token:window.sessionStorage.token},
            error: function () {
               // document.location.href = "admin_order-list.html"

            },
			success: function (d) {
				locDetails = d;

				$("#testimonial").val(d.testimonial);

				locCord = {lat:d.lat,lng:d.lng};

				if(google)

					marker = new google.maps.Marker({
			          position: {lat : Number(d.lat), lng : Number(d.lng)},
			          map: map,
			          title: d.installAt + "\n" + d.dataOfInstallation
			        })

			}
		});


		$("#updateLocation").on("click",function (e) {

			var data = {
				_id : id,
				token : window.sessionStorage.token,
				installAt : requirement.installAt || "residential",
				sysSize : assessment.sys_size,
				instalationCompletedOn : installation.time_ln[6] && installation.time_ln[6].ed_time,
				annual_consumption: assessment.annual_consumption,
				testimonial:$("#testimonial").val(),
				lat : locCord.lat,
				lng : locCord.lng
			}

			$.ajax({
				url:"/api/location",
				type:"POST",
				data:data,
	            error: function () {
	                $.ajax({
						url:"/api/location/"+id,
						type:"PATCH",
						data:data,
			            error: function () {
			                //document.location.href = "admin_order-list.html"

			            },
						success: function (d) {
							showNotification("Success ! Location was updated.", "success")
						}
					});

	            },
				success: function (d) {
					showNotification("Success ! Location was updated.", "success")
				}
			});	

		});
		$("#save").on("click",function (e) {
			var tl = [0,1,2,3,4,5,6,7,8].map(function (i) {
				return {
						name    : $(".name").eq(i).html(),
						desc    : $(".desc").eq(i).val(),
						st_time : $(".st_time").eq(i).val(),
						ed_time : $(".ed_time").eq(i).val(),
						status  : $(".status").eq(i).val(),
					};
			})
			var data = {
				_id : id,
				token : window.sessionStorage.token,
				time_ln : tl
			}


			$.ajax({
				url:"/api/installation",
				type:"POST",
				data:data,
	            error: function () {
	                $.ajax({
						url:"/api/installation/"+id,
						type:"PATCH",
						data:data,
			            error: function () {
			                document.location.href = "admin_order-list.html"

			            },
						success: function (d) {

							var status = $("#installationCompleted:checked").length ? "Installation_Completed" : "View_Installation_Design";
							$.ajax({
								url:'/api/requirement/'+id,
								type:"PATCH",
								data:{ "status" : status ,token : window.sessionStorage.token},
					            error: function () {
					            },
								success: function (d) {
									showNotification("Success ! Timeline details are updated", "success");
									toggleMap(d);
								}
							});
							installation = d;
						}
					});

	            },
				success: function (d) {
					$.ajax({
						url:'/api/requirement/'+id,
						type:"PATCH",
						data:{ "status" : "View_Installation_Design" ,token : window.sessionStorage.token},
			            error: function () {
			            },
						success: function (d) {
							showNotification("Success ! Timeline details are updated", "success")
						}
					});
					installation = d;
				}
			});			
		})


	    var requirement;
		
		$.ajax({
			url:"/api/requirement/"+id,
			type:"POST",
			data:{token:window.sessionStorage.token},
		    error: function () {
		        document.location.href = "user_dashboard.html"

		    },
			success: function (d) {

				requirement = d;


				var Current_average_energy_bill = Number(requirement.current_avg_bill || 0);
				var Available_rooftop_area = Number(requirement.rooftop_area || 0);


				var fac1 = 1;
				if(Current_average_energy_bill>=0 && Current_average_energy_bill<=5000){
				   fac1 = 7.5
				}else if(Current_average_energy_bill>=5001 && Current_average_energy_bill<=10000){
					fac1 = 7.9
				}else if(Current_average_energy_bill>=10001 && Current_average_energy_bill<=15000){
					fac1 = 8
				}else if(Current_average_energy_bill>=15001 && Current_average_energy_bill<=20000){
					fac1 = 8.2
				}else if(Current_average_energy_bill>=20001 && Current_average_energy_bill<=25000){
					fac1 = 8.5
				}else{
					fac1 = 8.7
				}

				var You_current_energy_need_is = (Current_average_energy_bill / fac1)*12;

				var c1 = 1350;
				var c2 = 0.9;

				 system_size_of1 = Math.round(Available_rooftop_area /100)
				 system_size_of2 = Math.round(You_current_energy_need_is * c2/c1)
				 system_size_of = Math.min(system_size_of1,system_size_of2)

				 requirement.system_size_of = system_size_of;

				
				$("#UserDetails").html(d.owner_salutation + ". "+ d.owner_fname+ " "+ d.owner_lname + " - " + d.location)
				$("#orderNo").html(d.orderid)


				if(d.status == "Installation_Completed"){
					$("#installationCompleted")[0].setAttribute("checked","checked");
				}
				toggleMap(d)
				
			}
		});


		$.ajax({
			url:"/api/assessment/"+id,
			type:"POST",
			data:{token:window.sessionStorage.token},
		    error: function () {
		        document.location.href = "user_dashboard.html"
		    },
			success: function (d) {
				assessment = d;
			}
		});
		
		for(var i=1; i<19;i++){
			$('#timeLinedate-cnt-'+i).monthly({
				mode        : 'picker',
				target      : '#timeLinedate-'+i,
				setWidth    : '250px',
				startHidden : true,
				showTrigger : '#timeLinedate-'+i,
				stylePast   : true,
				disablePast : true
			});
			
		}

	})

}]);