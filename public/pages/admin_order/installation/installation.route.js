'use strict';

angular.module("eightMinutes")
  .config(["$stateProvider",function($stateProvider) {
  	$stateProvider
  		.state("admin.order.installation", {
  			url : "/installation?id",
  			templateUrl : "./pages/admin_order/installation/installation.html",
 			controller : "adminOrderInstallationController"
  		})
  }])
