'use strict';

angular.module("eightMinutes")
  .config(["$stateProvider",function($stateProvider) {
  	$stateProvider
  		.state("admin.order.designs", {
  			url : "/designs?id",
  			templateUrl : "./pages/admin_order/designs/designs.html",
 			controller : "adminOrderDesignsController"
  		})
  }])
