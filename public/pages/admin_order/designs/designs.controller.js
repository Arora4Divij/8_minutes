angular.module('eightMinutes')
	.controller('adminOrderDesignsController',["$scope","$state",function($scope,$state){
			

	$(function(){
		
		id = $state.params.id;

		if(!id){
            document.location.href = "admin_order-list.html"
		}

        var installer_id,location,username;

		$.ajax({
			url:"/api/installation/"+id,
			type:"POST",
			data:{token:window.sessionStorage.token},
            error: function () {
                document.location.href = "admin_order-list.html"

            },
			success: function (d) {
				if(d && d.design){
					for(var i in d.design){
						$(".design-image").eq(Number(i)).attr("src", d.design[i]['img'] && d.design[i]['img']['file']);
						$(".design-image").eq(Number(i)).attr("alt", d.design[i]['img'] && d.design[i]['img']['ext']);
					}
				}

			}
		});

		$.ajax({
			url:"/api/requirement/"+id,
			type:"POST",
			data:{token:window.sessionStorage.token},
            error: function () {
                document.location.href = "admin_order-list.html"
            },
			success: function (d) {
				username = d.username;
				location = d.location;

				$("#UserDetails").html(d.owner_salutation + ". "+ d.owner_fname+ " "+ d.owner_lname + " - " + d.location)
				$("#orderNo").html(d.orderid)

			}
		});


		$("#save").on("click",function (e) {
			var data = {
				_id : id,
				username:username,
				location:location,
				updated_on:(new Date()).toString(),
				token : window.sessionStorage.token,
				design : [
					{
						desc : "Installation Design",
						img :{
							file :  $(".design-image").eq(0).attr("src"),
							ext :  $(".design-image").eq(0).attr("alt")
						}
					},
					{
						desc : "Arch. Drawing",
						img :{
							file :  $(".design-image").eq(1).attr("src"),
							ext :  $(".design-image").eq(1).attr("alt")
						}
					},
					{
						desc : "Wiring",
						img :{
							file :  $(".design-image").eq(2).attr("src"),
							ext :  $(".design-image").eq(2).attr("alt")
						}
					},
					{
						desc : "Arch. Drawing",
						img :{
							file :  $(".design-image").eq(3).attr("src"),
							ext :  $(".design-image").eq(3).attr("alt")
						}
					}
				]
			}


			$.ajax({
				url:"/api/installation",
				type:"POST",
				data:data,
	            error: function () {
	                $.ajax({
						url:"/api/installation/"+id,
						type:"PATCH",
						data:data,
			            error: function () {
			                document.location.href = "admin_order-list.html"

			            },
						success: function (d) {
							showNotification("Success ! Design data is updated", "success")	
						}
					});

	            },
				success: function (d) {
					showNotification("Success ! Design data is updated", "success")	
				}
			});			
		})


		$('[type="file"]').change(function(e){
		    if (this.files && this.files[0]) {
		        var reader = new FileReader();

		        var self = this;
		        reader.onload = function (e) {
		            $(self).prev().attr('src', e.target.result);
		            $(self).prev().attr('alt', $(self).val().split(".").pop());
		        }

		        reader.readAsDataURL(this.files[0]);
		    }
		});

});
}]);