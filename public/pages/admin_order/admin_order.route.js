'use strict';

angular.module("eightMinutes")
  .config(["$stateProvider",function($stateProvider) {
  	$stateProvider
  		.state("admin.order", {
  			url : "/order",
  			templateUrl : "./pages/admin_order/admin_order.html",
 			controller : "adminOrderController"
  		})
  }])
