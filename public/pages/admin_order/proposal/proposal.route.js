'use strict';

angular.module("eightMinutes")
  .config(["$stateProvider",function($stateProvider) {
  	$stateProvider
  		.state("admin.order.proposal", {
  			url : "/proposal?id",
  			templateUrl : "./pages/admin_order/proposal/proposal.html",
 			controller : "adminOrderProposalController"
  		})
  }])
