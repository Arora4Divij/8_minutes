angular.module('eightMinutes')
	.controller('adminOrderProposalController',["$scope","$state",function($scope,$state){
		

		
	var id = $state.params.id;



	$.ajax({
			url:"/api/requirement/"+id,
			type:"POST",
			data:{token:window.sessionStorage.token},
            error: function () {
                document.location.href = "user_dashboard.html"

            },
			success: function (d) {
				
				$("#UserDetails").html(d.owner_salutation + ". "+ d.owner_fname+ " "+ d.owner_lname + "&nbsp;-&nbsp;" + d.location)
				$("#orderNo").html(d.orderid)

				
			}
		});

	$.ajax({
		url:'/api/assessment/'+id,
		type:"POST",
		data:{token : window.sessionStorage.token},
        error: function () {
	        
			document.location.href = "admin_order-list.html"

        },
		success: function (d) {
			
			$("#utility").val(d.utility)
			$("#present_tariff").val(d.present_tariff)
			$("#annual_bill").val(d.annual_bill)
			$("#annual_consumption").val(d.annual_consumption)
			$("#sys_size").val(d.sys_size)
			$("#roof_area").val(d.roof_area)
			$("#est_anual_prod").val(d.est_anual_prod)
			$("#enery_mix_solar").val(d.enery_mix_solar)
			$("#enery_mix_grid").val(d.enery_mix_grid)
			
			$("#inverter").val(d.inverter)
			$("#systemkit").val(d.systemkit)
			$("#module_specifics").val(d.module_specifics)
			$("#structure").val(d.structure)
			
			$("#env_co2_impact").val(d.env_co2_impact)
			$("#equ_driving").val(d.equ_driving)
			$("#equ_carbon").val(d.equ_carbon)
			$("#equ_coal").val(d.equ_coal)

			$("#consumption input").each(function(i){
				$(this).val(d.consumption[i] || "")
			});	

			$("#cost input").each(function(i){
				$(this).val(d.cost[i] || "")
			});	

			$("#fixedCost input").each(function(i){
				$(this).val(d.fixed_cost[i] || "")
			});	

		}
	});

	$("#enery_mix_solar").on("keyup",function () {
		$("#enery_mix_grid").val(100- Number($(this).val()));
	});
	$("#enery_mix_grid").on("keyup",function () {
		$("#enery_mix_solar").val(100- Number($(this).val()));
	});	

	$("#submit").on("click",function () {
		var consumption = [];
		var cost = [];
		var fixedCost = [];

		for(var i =0; i<$("#consumption input").size(); i++){
			consumption.push( $("#consumption input").eq(i).val())
		}
		for(var i =0; i<$("#cost input").size(); i++){
			cost.push( $("#cost input").eq(i).val())
		}
		for(var i =0; i<$("#fixedCost input").size(); i++){
			fixedCost.push( $("#fixedCost input").eq(i).val())
		}

		var data = {
			_id                : id,
			utility            : $("#utility").val(),
			present_tariff     : $("#present_tariff").val(),
			annual_bill        : $("#annual_bill").val(),
			annual_consumption : $("#annual_consumption").val(),
			
			consumption        : consumption,
			cost               : cost,
			fixed_cost         : fixedCost,
			
			sys_size           : $("#sys_size").val(),
			roof_area          : $("#roof_area").val(),
			est_anual_prod     : $("#est_anual_prod").val(),
			enery_mix_solar    : $("#enery_mix_solar").val(),
			enery_mix_grid     : $("#enery_mix_grid").val(),
			
			inverter           : $("#inverter").val(),
			systemkit          : $("#systemkit").val(),
			module_specifics   : $("#module_specifics").val(), 
			structure          : $("#structure").val(), 
			
			env_co2_impact     : $("#env_co2_impact").val(),
			equ_driving        : $("#equ_driving").val(),
			equ_carbon         : $("#equ_carbon").val(),
			equ_coal           : $("#equ_coal").val(),
			token              : window.sessionStorage.token
		};


		$.ajax({
			url:'/api/assessment',
			type:"POST",
			data:data,
            error: function () {
		        $.ajax({
					url:'/api/assessment/'+id,
					type:"PUT",
					data:data,
		            error: function () {
				        document.location.href = "admin_order-list.html"
		            },
					success: function (d) {
						document.location.href='admin_order-proposal1.html#'+id;
					}
				});

            },
			success: function (d) {
				document.location.href='admin_order-proposal1.html#'+id;
			}
		});
			



	})
	

    $("#consumption input").on("change",function(){
    	var sum = 0;
    	for(var i = 0; i<12;i++){
    		sum += (+($("#consumption input").eq(i).val() ||0))
    	
    	}
    	$("#annual_consumption").val(sum)
    })
	
    $("#cost input").on("change",function(){
    	var sum = 0;
    	for(var i = 0; i<12;i++){
    		sum += (+($("#cost input").eq(i).val() ||0))
    	
    	}
    	$("#annual_bill").val(sum)
    })



}]);