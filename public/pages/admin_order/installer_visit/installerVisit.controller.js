angular.module('eightMinutes')
	.controller('adminOrderInstallerVisitController',["$scope","$state",function($scope,$state){
		

		$(function(){

		var	id = $state.params.id;

		if(!id){
            //document.location.href = "admin_order-list.html"
		}

        var installer_id;



        $.ajax({
			url:"/api/feedback/" + id,
			type:"POST",
			data:{ token : window.sessionStorage.token },
            error: function () {
		        //document.location.href = "admin_order-list.html"
            },
			success: function (fb) {
				$("#inst-feedback").html(fb.inst_feedback)
			}
		});

		function updateInstallerDetails(d){
			$.ajax({
				url:"/api/installer/" + d.installer_id,
				type:"POST",
				data:{token:window.sessionStorage.token},
	            error: function () {
			        //document.location.href = "admin_order-list.html"
	            },
				success: function (installer) {
					$("#installer-name").html(installer.name)
					$("#installer-email").html(installer.email)
					$("#installer-company").html(installer.companyname)
					$("#installer-phone").html(installer.phone)
				}
			});
		}

		$.ajax({
			url:"/api/requirement/"+id,
			type:"POST",
			data:{token:window.sessionStorage.token},
            error: function () {
                //document.location.href = "admin_order-list.html"

            },
			success: function (d) {

				$("#act-visit-time").html((dateFormat(d.act_visit_date) || "")+", "+(d.act_visit_time || ""))
				$("#req-visit-time").html(dateFormat(d.req_visit_date)+", "+(d.req_visit_time))

				if(d.installer_id){
					updateInstallerDetails(d)
					$("#assignInst").html("Re-Assign Installer")
				}

				$("#UserDetails").html(d.owner_salutation + ". "+ d.owner_fname+ " "+ d.owner_lname + " - " + d.location)
				$("#orderNo").html(d.orderid)

			}
		});


		$.ajax({
			url:"/api/feedback/" + id,
			type:"POST",
			data:{token:window.sessionStorage.token},
            error: function () {
		        //document.location.href = "admin_order-list.html"
            },
			success: function (fb) {
				if(fb){
					$("#vq1").html(fb.vq1);
					$("#vq2").html(fb.vq2);
					$("#vq3").html(fb.vq3);
					$("#vq4").html(fb.vq4);
					$("#vq5").html(fb.vq5);
					$("#vq6").html(fb.vq6);
					$("#vq7").html(fb.vq7);
					$("#vq8").html(fb.vq8);

					$("#iq1").html(fb.iq1);
					$("#iq2").html(fb.iq2);
					$("#iq3").html(fb.iq3);
					$("#iq4").html(fb.iq4);
					$("#iq5").html(fb.iq5);
					$("#iq6").html(fb.iq6);
					$("#iq7").html(fb.iq7);
					$("#iq8").html(fb.iq8);

					$("#cust-feedback").html(fb.cust_feedback);
					$("#inst-feedback").html(fb.inst_feedback);
				}
			}
		});

		



		$("#company").on("change", function () {
			


			$.ajax({
				url:"/api/installer/all",
				type:"POST",
				data:{company_id:$(this).val(),token:window.sessionStorage.token},
	            error: function () {
			        //document.location.href = "admin_order-list.html"
	            },
				success: function (list) {
					$("#installer").html("<option value=''>select</option>")
					for(var i in list){
						var item = list[i];
						var $elm = $("<option value=\""+ item._id + "\">"+ item.name +"</option>");
						$("#installer").append($elm)
					}
					
				}
			});

		});

		$("#assign").on("click", function () {
			var val = $("#installer").val();
			if(val)
			$.ajax({
				url:"/api/requirement/"+id,
				type:"PATCH",
				data:{installer_id:val,token:window.sessionStorage.token},
	            error: function () {
			        //document.location.href = "admin_order-list.html"
	            },
				success: function (d) {
					updateInstallerDetails(d);
					$('#myModal').modal("hide");
				}
			});

		})

		$("#create-prop").on("click",function () {
			document.location.href = "admin_order-proposal.html#" + id;
		});

		/*$("#allowUsrFB").on("click",function () {
			$.ajax({
				url:"/api/requirement/"+id,
				type:"PATCH",
				data:{status:"visiter_visited",token:window.sessionStorage.token},
	            error: function () {
			        //document.location.href = "admin_order-list.html"
	            },
				success: function (fb) {
					$("#cust-feedback").html(fb.cust_feedback)
					$("#inst-feedback").html(fb.inst_feedback)
				}
			});
		});*/


		$.ajax({
			url:"/api/company/all",
			type:"POST",
			data:{token:window.sessionStorage.token},
            error: function () {
		        //document.location.href = "admin_order-list.html"
            },
			success: function (list) {
				$("#company").html("<option value=''>select</option>")
				for(var i in list){
					var item = list[i];
					var $elm = $("<option value=\""+ item._id + "\">"+ item.name +"</option>");
					$("#company").append($elm)
				}
				
			}
		});

	})


}]);