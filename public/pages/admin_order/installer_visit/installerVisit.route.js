'use strict';

angular.module("eightMinutes")
  .config(["$stateProvider",function($stateProvider) {
  	$stateProvider
  		.state("admin.order.installerVisit", {
  			url : "/installerVisit?id",
  			templateUrl : "./pages/admin_order/installer_visit/installerVisit.html",
 			controller : "adminOrderInstallerVisitController"
  		})
  }])
