'use strict';

angular.module("eightMinutes")
  .config(["$stateProvider",function($stateProvider) {
  	$stateProvider
  		.state("admin.orders", {
  			url : "/orders",
  			templateUrl : "./pages/admin_orders/admin_orders.html",
 			controller : "adminOrdersController"
  		})
  }])
