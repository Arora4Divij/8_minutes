angular.module('eightMinutes')
	.controller('adminOrdersController',["$scope","$state",function($scope,$state){
			
			var userList,ud;
	
	$(function(){

		if(!window.sessionStorage.token){
			$("#myModal2").modal("show")
		}
		
		$("#addOrder").on("click",function(){
			$("#myModal3").modal("show")
		})
		
		$("#addtoMap").on("click",function(){
			$("#myModal4").modal("show")
		})

		$('#visit_date_cnt').monthly({
			mode        : 'picker',
			target      : '#visit_date',
			setWidth    : '250px',
			startHidden : true,
			showTrigger : '#visit_date',
			stylePast   : true,
			disablePast : true
		});

		$.ajax({
			url:"/api/user/all",
			type:"POST",
			data:{token:window.sessionStorage.token},
			success: function (list) {
				userList = list
				for(var i in list){
					var item = list[i];
					$("#userDetails").append($("<option value='"+  item.email  +"'></option>"))
				}
				$(".select2").select2();
			}
		});


		$("#isOwner").on("click",function () {
			if ($(this).is(':checked')&& ud) {
	           	$("#first_name").val(ud.fname);
				$("#last_name").val(ud.lname);
				$("#phone").val(ud.phone);
				
				$("#email").val(ud.email);
	        }else{
	        	$("#first_name").val("");
				$("#last_name").val("");
				$("#phone").val("");
				
				$("#email").val("");	
	        }
		})
		$("#sameAsMyAddress").on("click",function () {
			if ($(this).is(':checked') && ud) {
	           
				//$("#location").val(ud.location);
				$("#Address_line_1").val(ud.address1);
				$("#Address_line_2").val(ud.address2);
				$("#pin").val(ud.pin);
	        }else{
				//$("#location").val("");
				$("#Address_line_1").val("");
				$("#Address_line_2").val("");
				$("#pin").val("");
	        }
		})

		$("#userdetails_input").on("change",function () {
			var email = $(this).val();
			ud = userList.filter(function(a){
				return a.email == email
			})[0]
			if(email && !ud){
				$("#password-cnt").show()
				$("#password").val("")
				$("#password").attr("required","required")
			}else{
				$("#password-cnt").hide()
				$("#password").removeAttr("required")
			}
		})


		$("#logIn").on("click", function(){
		    

		    $.ajax({
				url:"/api/login",
				type:"POST",
				data:{
				    	"username" : $("#instEmail").val(),
				    	"password" :$("#instPassword").val()
				    },
				success: function (d) {
					window.sessionStorage.token = (d.token)
					$("#myModal2").modal("hide");
					loadOrderList()
				}
			})

		})

		function loadOrderList(){


			$.ajax({
				url:"/api/requirement/all",
				type:"POST",
				data:{token:window.sessionStorage.token},
				error:function(e){
					if(e && e.status == 403){
						delete window.sessionStorage.token;
						//window.location.reload();
					}
				},
				success: function (d) {
					for (var i in d){
						var item = d[i]
						
						var $elm = $([
								"<tr>",
									"<td><a href=\"admin/order/details?id="+ item._id +"\">", item.orderid||i ,"</a></td>",
									// "<td><a ng-click='goToDetails("+ item._id +")'"+"\">", item.orderid||i ,"</a></td>",
									"<td>" + item.itemname.split(" : ")[0] + "</td>",
									"<td>", item.owner_salutation , " ", item.owner_fname, " ", item.owner_lname ,"</td>",
									"<td class=\"text-left\">", item.location ,"</td>",
									"<td class=\"text-left\">", item.installername ,"</td>",
									"<td class=\"text-left\">", dateFormat(item.req_visit_date) , "<br>", item.req_visit_time ,"</td>",
									"<td class=\"text-left\">", dateFormat(item.act_visit_date) , "<br>", item.act_visit_time ,"</td>",
									"<td class=\"text-left\">", item.status || "Pending" ,"</td>",
									"<td class=\"text-left\">", item.addedby ,"</td>",
								"</tr>"
							].join(''));
						$("#orderList").append($elm)
					}

					$("#myTable").tablesorter(); 
				}
			})
		}
		loadOrderList();
	});

	$scope.goToDetails = function(id){
		console.log(id);
	}


	$("#userOrder").on("submit",function (argument) {
		var location       = $("#location").val();
		var first_name     = $("#first_name").val();
		var last_name      = $("#last_name").val();
		var phone          = $("#phone").val();
		var Address_line_1 = $("#Address_line_1").val();
		var Address_line_2 = $("#Address_line_2").val();
		var email          = $("#email").val();
		var visit_date     = $("#visit_date").val();
		var visit_time     = $("#visit_time").val();
		var pin            = $("#pin").val();
		var salutation     = $("#salutation").val();
		var isOwner        = $("#isOwner").val();
		var area           = $("#area").val();
		var bill           = $("#bill").val();
		var installAt      = $("[name='locationType']:checked").val();


		var data = {
			rooftop_area     : area,
			current_avg_bill : bill,
			installAt        : installAt,
			req_visit_time   : visit_time,
			req_visit_date   : visit_date,
			address_l1       : Address_line_1,
			address_l2       : Address_line_2,
			location         : location,
			pin              : pin,
			owner_salutation : salutation,
			owner_fname      : first_name,
			owner_lname      : last_name,
			username         : ud ? ud.username : $("#userdetails_input").val(),
			owner_phone      : phone,
			owner_email      : email
		}
		
		data["token"] = window.sessionStorage.token;

		if($("#password").is(":visible")){
			var userData = {
				username:$("#userdetails_input").val(),
				password:$("#password").val(),
				salutation:salutation,
				fname:first_name,
				lname:last_name,
				address1:Address_line_1,
				address2:Address_line_2,
				email:email,
				phone:phone,
				token:window.sessionStorage.token
			}
			$.ajax({
				url:"/api/register",
				type:"POST",
				data:userData,
				success: function (item) {
					saveReq(data,ud);
				}
			});
		}else{
			saveReq(data)
		}
		
	});

	function saveReq(data){
		

		$.ajax({
			url:"/api/requirement",
			type:"POST",
			data:data,
			success: function (item) {
				$("#myModal3").modal("hide");
				window.location.href = "admin_order-details.html#"+item._id;

				$("#userOrder").reset();
			}
		});
	}

	setTimeout(function(){$("#password").val("")},5000)

	}]);


	var marker;
			    		var map;
			    		var locCord = {};
			    		var locDetails;
			    		var locationId;
			    	  var isFirstTime = true;
					  function initAutocomplete() {

					  	if(!isFirstTime){
					  		return;
					  	}
						
						isFirstTime = false;

					    map = new google.maps.Map(document.getElementById('map'), {
					       zoom: 10,
				          //mapTypeId: 'satellite',
				          //mapTypeId: google.maps.MapTypeId.ROADMAP,
				          center: {lat : 28.5248825, lng : 77.190029}
					    });


					    // Create the search box and link it to the UI element.
					    var input = document.getElementById('pac-input');
					    var searchBox = new google.maps.places.SearchBox(input);
					    map.controls[google.maps.ControlPosition.TOP_LEFT].push(input);

					    // Bias the SearchBox results towards current map's viewport.
					    map.addListener('bounds_changed', function() {
					      searchBox.setBounds(map.getBounds());
					    });

					    var markers = [];
					    // Listen for the event fired when the user selects a prediction and retrieve
					    // more details for that place.
					    searchBox.addListener('places_changed', function() {
					      var places = searchBox.getPlaces();

					      if (places.length == 0) {
					        return;
					      }

					      // Clear out the old markers.
					      markers.forEach(function(marker) {
					        marker.setMap(null);
					      });
					      markers = [];

					      // For each place, get the icon, name and location.
					      var bounds = new google.maps.LatLngBounds();
					      places.forEach(function(place) {
					        if (!place.geometry) {
					          console.log("Returned place contains no geometry");
					          return;
					        }
					        var icon = {
					          url: place.icon,
					          size: new google.maps.Size(71, 71),
					          origin: new google.maps.Point(0, 0),
					          anchor: new google.maps.Point(17, 34),
					          scaledSize: new google.maps.Size(25, 25)
					        };

					        // Create a marker for each place.
					        markers.push(new google.maps.Marker({
					          map: map,
					          icon: icon,
					          title: place.name,
					          position: place.geometry.location
					        }));

					        if (place.geometry.viewport) {
					          // Only geocodes have viewport.
					          bounds.union(place.geometry.viewport);
					        } else {
					          bounds.extend(place.geometry.location);
					        }
					      });
					      map.fitBounds(bounds);
					    });

					    	if(locDetails){
						    	marker = new google.maps.Marker({
						          position: {lat : Number(locDetails.lat), lng : Number(locDetails.lng)},
						          map: map,
						          title: locDetails.installAt + "\n" + locDetails.dataOfInstallation
						        })
					    	}
					    	map.addListener('click', function(e) {
								
								
								locCord = {lat:e.latLng.lat(),lng:e.latLng.lng()};
								addMarker(locCord)

							  });
					  }

					  function addMarker(locCord){
					  	if(marker){

							var latlng = new google.maps.LatLng(+locCord.lat, +locCord.lng);
							marker.setPosition(latlng);
						}else{
							marker = new google.maps.Marker({
					          position: {lat : +locCord.lat, lng : +locCord.lng},
					          map: map
					        })
						}
					  }


					  var locationList;

					  function init (){

						  $.ajax({
								url:"/api/location/all",
								type:"POST",
								data:{token:window.sessionStorage.token},
					            error: function () {
					               // document.location.href = "admin_order-list.html"

					            },
								success: function (list) {
									locationList = list;
									setTimeout(initAutocomplete,2000)
									$("#locationListCnt").html("")
									for(var i in list){
										var item = list[i] 
										$("#locationListCnt").append([
											"<tr>",
												"<td>", item.installAt ,"</td>",
												"<td>", item.sysSize ,"</td>",
												"<td>", item.lat ,"</td>",
												"<td>", item.lng ,"</td>",
												"<td>", item.testimonial ,"</td>",
												"<td>", dateFormat(item.instalationCompletedOn) ,"</td>",
												"<td>", item.annual_consumption ,"</td>",
												"<td><a class='edit' href=\"", item._id ,"\">Edit</a><br><a href=\"", item._id ,"\" class='delete'>Delete</a></td>",											
											"</tr>"
										].join(""))
									}
									regEvents()
								}
							});
						}

						function regEvents(){

							$("#locationListCnt a.edit").on("click", function(e){
								e.preventDefault();
								locationId = $(this).attr("href")
								loc = locationList.filter(function(a){
									return locationId == a._id
								})[0]

								loc && addMarker(loc);

								for(var i in loc){
									$("#"+i).val(loc[i])
								}

								locCord.lat = +loc.lat
								locCord.lng = +loc.lng

								return false;
							})
							$("#locationListCnt a.delete").on("click", function(e){
									e.preventDefault();
									locationId = $(this).attr("href")
								 $.ajax({
									url:"/api/location/"+locationId+"/delete",
									type:"POST",
									data:{token:window.sessionStorage.token},
						            error: function () {
						                //document.location.href = "admin_order-list.html"

						            },
									success: function (d) {
										showNotification("Success ! Location was updated.", "success")
										init()
									}
								});


								return false;
							});
						}

						$("#addNewPin").on("click", function(){
							loc = locationList.filter(function(a){
								return locationId == a._id
							})[0]

							loc.instalationCompletedOn = formatDate2(loc.instalationCompletedOn)

							for(var i in loc){
								$("#"+i).val("")
							}
							locationId = ""

						})

						

						$("#updateLocation").on("click",function (e) {

							var data = {
								token : window.sessionStorage.token,
								installAt : $("#installAt").val(),
								sysSize : $("#sysSize").val(),
								instalationCompletedOn :$("#instalationCompletedOn").val(),
								annual_consumption: $("#annual_consumption").val(),
								testimonial:$("#testimonial").val(),
								lat : locCord.lat,
								lng : locCord.lng
							}

							if(locationId){
								data["_id"] = locationId
							}

							$.ajax({
								url:"/api/location",
								type:"POST",
								data:data,
					            error: function () {
					                $.ajax({
										url:"/api/location/"+locationId,
										type:"PATCH",
										data:data,
							            error: function () {
							                //document.location.href = "admin_order-list.html"

							            },
										success: function (d) {
											showNotification("Success ! Location was updated.", "success")
											init()
										}
									});

					            },
								success: function (d) {
									showNotification("Success ! Location was updated.", "success")
									init()
								}
							});	

						});


