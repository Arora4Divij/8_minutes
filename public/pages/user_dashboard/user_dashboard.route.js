'use strict';

angular.module("eightMinutes")
  .config(["$stateProvider",function($stateProvider) {
  	$stateProvider
  		.state("user.dashboard", {
  			url : "/dashboard",
  			templateUrl : "./pages/user_dashboard/user_dashboard.html",
 			controller : "userDashboardController"
  		})
  }])
