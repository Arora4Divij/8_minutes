angular.module('eightMinutes')
	.controller('userDashboardController',["$scope",function($scope){


			

$(function () {
	//Initialize Select2 Elements
	$(".select2").select2();
});

$(function(){

		$.ajax({
			url:"/api/requirement/all",
			type:"POST",
			data:{token:window.sessionStorage.token},
			success: function (d) {
				for (var i in d){
					var item = d[i]
					var status = item.status || "pending";

					switch(status){

						case "pending" : { // default status
						}
						case "visiter_assigned":{
						}
						case "visiter_visited":{
						}
						case "view_proposel":{
						}
						case "View_Proposal":{
							status = "<a href=\"user_schedule.html#"+item.
							_id+"\">"+status.replace(/\_/gi, " ")+"</a>"
							break;
						}
						case "View_Contract":{
							status = "<a href=\"user_contract.html#"+item.
							_id+"\">"+status.replace(/\_/gi, " ")+"</a>"
							break;
						}
						case "View_Contract_Summary":{
							status = "<a href=\"user_contract1.html#"+item.
							_id+"\">"+status.replace(/\_/gi, " ")+"</a>"
							break;
						}
						case "payment_complete":{
							status = "<a href=\"user_contract1.html#"+item.
							_id+"\">"+status.replace(/\_/gi, " ")+"</a>"
							break;
						}
						case "View_Installation_Design":{
							status = "<a href=\"user_installation.html#"+item.
							_id+"\">"+status.replace(/\_/gi, " ")+"</a>"
							break;
						}
						case "View_Installation_Progress":{
							status = "<a href=\"user_installation1.html#"+item.
							_id+"\">"+status.replace(/\_/gi, " ")+"</a>"
							break;
						}

					}

					var $elm = $([
							"<tr>",
								"<td>", (item.orderid || "") ,"</td>",
								"<td class=\"text-left\">", item.location ,"</td>",
								"<td class=\"text-left\">", status ,"</td>",
							"</tr>"
						].join(''));
				/*<td>1</td>
				<td class="text-left">Kutub Plaza</td>
				<td class="text-left"><a href="user_contract.html" class="black">Contract pending</a></td>*/
					$("#orderList").append($elm)
				}
				$("#orderList a").on("click", function () {
					window.sessionStorage.id = $(this).attr("href").split("#")[1];
				})
			}
		});



		$.ajax({
			url:"/api/location/all",
			type:"POST",
			data:{token:window.sessionStorage.token},
            error: function () {

            },
			success: function (list) {
				var marker = [];
				var infowindow = [];
				var activeInfoWndow;
				var contentString;
				for(var i in list){
					d = list[i]
					if(google)
						var iconUrl = (d.installAt && d.installAt.toLowerCase()) ==="residential" ? "http://maps.google.com/mapfiles/ms/icons/red-dot.png":"http://maps.google.com/mapfiles/ms/icons/blue-dot.png"

						contentString = '<div id="content">'+
				            '<div id="siteNotice">'+
				            '</div>'+
				            '<div id="bodyContent">'+
				            	'<table>'+
				            		'<tr>'+
				            			'<td>System Size</td>'+
				            			'<td>'+d.sysSize+'</td>'+
				            		'</tr>'+
				            		'<tr>'+
				            			'<td>Annual Consumption</td>'+
				            			'<td>'+d.annual_consumption+'</td>'+
				            		'</tr>'+
				            		'<tr>'+
				            			'<td>Instalation Completed On &nbsp;&nbsp;</td>'+
				            			'<td>'+d.instalationCompletedOn+'</td>'+
				            		'</tr>'+
				            		'<tr>'+
				            			'<td>Location type</td>'+
				            			'<td>'+d.installAt+'</td>'+
				            		'</tr>'+
				            		'</tr>'+
				            	'</table>'+
				            	'<hr><b>Testimonial</b><p>'+ d.testimonial +'</p>'+
				            '</div>'+
				            '</div>';

				        infowindow[i] = new google.maps.InfoWindow({
				          content: contentString
				        });

						marker[i]=new google.maps.Marker({
				          position: {lat : Number(d.lat), lng : Number(d.lng)},
				          map: map,
				          icon: iconUrl,
				          title: "Location type: " + d.installAt + "\nSystem Size: " + d.sysSize
				        });

				        (function(i){
					        marker[i].addListener('click', function() {

					          if(activeInfoWndow){activeInfoWndow.close()}
					          activeInfoWndow = infowindow[i];
					          activeInfoWndow.open(map, marker[i]);
					        });
				        })(i)
				}

			}
		});

	})

	$('#loadVideo').click(function(){
		$('#videoContainer').html('<iframe width="640" height="360" src="https://www.youtube.com/embed/rsuyaQIFTz8?autoplay=1" frameborder="0" allowfullscreen id="embededVideo"></iframe>');

		// $('#embededVideo').attr('src','https://www.youtube.com/embed/rsuyaQIFTz8?autoplay=1');
	})
	$('#videoModal').on('hidden.bs.modal', function () {
		$('#embededVideo').remove();
	})


	}]);	 


var map;
	      function initMap() {
	        var uluru = {lat: -25.363, lng: 131.044};
	        map = new google.maps.Map(document.getElementById('map'), {
	         zoom: 10,
	          mapTypeId: 'satellite',
	          //mapTypeId: google.maps.MapTypeId.ROADMAP,
	          center: {lat : 28.5248825, lng : 77.190029}
	        });
	        
	        // Define the LatLng coordinates for the outer path.
	        var outerCoords = [
	          {lat: 17.864, lng: 72.848495}, // north west
	          {lat: 19.064, lng: 72.848495}, // south west
	          {lat: 19.064, lng: 74.348495}, // south east
	          {lat: 17.864, lng: 74.348495}  // north east
	        ];

	        // Define the LatLng coordinates for an inner path.
	        var innerCoords1 = [
	          {lat: -33.364, lng: 154.207},
	          {lat: -34.364, lng: 154.207},
	          {lat: -34.364, lng: 155.207},
	          {lat: -33.364, lng: 155.207}
	        ];

	        // Define the LatLng coordinates for another inner path.
	        var innerCoords2 = [
	          {lat: -33.364, lng: 156.207},
	          {lat: -34.364, lng: 156.207},
	          {lat: -34.364, lng: 157.207},
	          {lat: -33.364, lng: 157.207}
	        ];

	        //map.data.add({geometry: new google.maps.Data.Polygon([outerCoords])})

	        /*map.data.setStyle({
	          fillColor: 'red',
	          strokeWeight: 0
	        });*/

	        // Create the search box and link it to the UI element.
        var input = document.getElementById('pac-input');
        var searchBox = new google.maps.places.SearchBox(input);
        map.controls[google.maps.ControlPosition.TOP_LEFT].push(input);

        // Bias the SearchBox results towards current map's viewport.
        map.addListener('bounds_changed', function() {
          searchBox.setBounds(map.getBounds());
        });


        var markers = [];
        // Listen for the event fired when the user selects a prediction and retrieve
        // more details for that place.
        searchBox.addListener('places_changed', function() {
          var places = searchBox.getPlaces();

          if (places.length == 0) {
            return;
          }

          // Clear out the old markers.
          markers.forEach(function(marker) {
            marker.setMap(null);
          });
          markers = [];

          // For each place, get the icon, name and location.
          var bounds = new google.maps.LatLngBounds();
          places.forEach(function(place) {
            if (!place.geometry) {
              console.log("Returned place contains no geometry");
              return;
            }
            var icon = {
              url: place.icon,
              size: new google.maps.Size(71, 71),
              origin: new google.maps.Point(0, 0),
              anchor: new google.maps.Point(17, 34),
              scaledSize: new google.maps.Size(25, 25)
            };

            // Create a marker for each place.
            markers.push(new google.maps.Marker({
              map: map,
              icon: icon,
              title: place.name,
              position: place.geometry.location
            }));

            if (place.geometry.viewport) {
              // Only geocodes have viewport.
              bounds.union(place.geometry.viewport);
            } else {
              bounds.extend(place.geometry.location);
            }
          });
          map.fitBounds(bounds);
        });
      


	        map.addListener('click', function(e) {
			    console.log(e.latLng.lat());
			    console.log(e.latLng.lng());

				var latlng = new google.maps.LatLng(e.latLng.lat(), e.latLng.lng());
				markerT.setPosition(latlng);

			  });

}