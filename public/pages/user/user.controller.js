angular.module('eightMinutes')
	.controller('userController',["$scope","$state",function($scope,$state){
	 $scope.goTo  = function(location)
		{
			$state.go('admin.'+location)
			console.log('admin.'+location);
			makeActive(location);
		}

		$scope.logout = function(){
			console.log('logging out');
			window.sessionStorage.removeItem('token');
			$state.go('login');
		}
		// goTo('dsa');
		function removeActiveClass(){
			$('#orders').removeClass('navActive');
			$('#installer').removeClass('navActive');
			$('#companies').removeClass('navActive');
			$('#referrals').removeClass('navActive');
			$('#subAdmin').removeClass('navActive');

			$('#orders').removeClass('navInactive');
			$('#installer').removeClass('navInactive');
			$('#companies').removeClass('navInactive');
			$('#referrals').removeClass('navInactive');
			$('#subAdmin').removeClass('navInactive');

			$('#orders').addClass('navInactive');
			$('#installer').addClass('navInactive');
			$('#companies').addClass('navInactive');
			$('#referrals').addClass('navInactive');
			$('#subAdmin').addClass('navInactive');
		}

		function makeActive(id){
			removeActiveClass();
			// console.log('#'+id);
			$('#'+id).removeClass('navInActive');
			$('#'+id).addClass('navActive');
		}
	}]);