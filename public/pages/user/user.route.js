'use strict';

angular.module("eightMinutes")
  .config(["$stateProvider",function($stateProvider) {
  	$stateProvider
  		.state("user", {
  			url : "/user",
  			templateUrl : "./pages/user/user.html",
 			controller : "userController"
  		})
  }])
