'use strict';

angular.module("eightMinutes")
  .config(["$stateProvider",function($stateProvider) {
  	$stateProvider
  		.state("admin", {
  			url : "/admin",
  			templateUrl : "./pages/admin/admin.html",
 			controller : "adminController"
  		})
  }])
