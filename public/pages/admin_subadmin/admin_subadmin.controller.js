angular.module('eightMinutes')
    .controller('subAdminController', ["$scope", "$http", "$timeout", function($scope, $http, $timeout) {


        $scope.successMessage = false;
        $scope.errorMessage = false;
        $scope.legitUsername = true;
        $scope.legitPassword = true;
        $scope.legitSalutation = true;
        $scope.legitName = true;
        $scope.legitLastName = true;
        $scope.legitAddress1 = true;
        $scope.legitAddress2 = true;
        $scope.legitEmail = true;
        $scope.legitPhone = true;
        $scope.legitRole = true;
        $scope.allInputsFilled = [];

        $scope.data = {};
        $scope.data.function = {};
        $scope.data.function.designation = "na";
        $scope.data.token = sessionStorage.getItem("token");

        $scope.data.user = {};
        $scope.data.user.username = "";
        $scope.data.user.address1 = "";
        $scope.data.user.address2 = "";
        $scope.data.user.password = "";
        $scope.data.user.salutation = "ba";

        var reqData = $scope.data;
        reqData.token = $scope.data.token;


        $scope.console1 = function() {
            console.log(reqData);
            $http({
                method: 'POST',
                url: '/api/add-sub-admin',
                data: reqData
            }).then(function successCallback(response) {

                $scope.data = {};
                $scope.successMessage = true;
                $timeout(function() {
                    $scope.successMessage = false;
                    $scope.modalClose();
                }, 5000);
            }, function errorCallback(response) {

                $scope.errorMessage = true;
                $timeout(function() {
                    $scope.errorMessage = false;
                }, 5000);
            });
        }


        $scope.modalClose = function() {
            $('#myModal1').modal('hide');
        }


        $scope.checkUserName = function(username) {
            var re = /^[a-zA-Z0-9!@#\$%\^\&*\)\(+=._-]{6,}$/g;
            $scope.legitUsername = re.test(username);
            $scope.validForm(username, 0);
            $scope.finalValidation();
        }


        $scope.checkPassword = function(password) {
            if (password.length < 6) {
                $scope.legitPassword = false;
            } else {
                $scope.legitPassword = true;
            }
            $scope.validForm(password, 1);
            $scope.finalValidation();
        }

        $scope.checkSalutation = function(salutation) {
            if (salutation == "ba") {
                $scope.legitSalutation = false;
                $scope.allInputsFilled[2] = false;
            } else {
                $scope.legitSalutation = true;
                $scope.allInputsFilled[2] = true;
            }
            $scope.finalValidation();
        }

        $scope.checkName = function(name) {
            var re = /[A-Za-z]/;
            $scope.legitName = re.test(name);
            if (name == undefined) {
                $scope.legitName = false;
            }
            $scope.validForm(name, 3);
            $scope.finalValidation();
        }

        $scope.checkLastName = function(name) {
            var re = /[A-Za-z]/;
            $scope.legitLastName = re.test(name);
            if (name == undefined) {
                $scope.legitLastName = false;
            }
            $scope.validForm(name, 4);
            $scope.finalValidation();
        }

        $scope.checkAddress1 = function(address1) {
            if (address1.length == 0) {
                $scope.legitAddress1 = false;
            } else {
                $scope.legitAddress1 = true;
            }
            $scope.validForm(address1, 5);
            $scope.finalValidation();
        }

        $scope.checkAddress2 = function(address2) {
            if (address2.length == 0) {
                $scope.legitAddress2 = false;
            } else {
                $scope.legitAddress2 = true;
            }
            $scope.validForm(address2, 6);
            $scope.finalValidation();
        }

        $scope.checkEmail = function(email) {
            var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
            $scope.legitEmail = re.test(email);
            $scope.validForm(email, 7);
            $scope.finalValidation();
        }

        $scope.checkPhone = function(phone) {
            var re = /\+?\d[\d -]{8,12}\d/;
            $scope.legitPhone = re.test(phone);
            $scope.validForm("" + phone, 8);
            $scope.finalValidation();
        }

        $scope.checkRole = function(role) {
            if (role == "na") {
                $scope.legitRole = false;
                $scope.allInputsFilled[9] = false;
            } else {
                $scope.legitRole = true;
                $scope.allInputsFilled[9] = true;
            }
            $scope.finalValidation();
        }


        $scope.validForm = function(input, index) {
            if (input.length) {
                $scope.allInputsFilled[index] = true;
            } else {
                $scope.allInputsFilled[index] = false;
            }
        }

        $scope.finalValidation = function() {
            var validFlag = false;
            for (var i = 0; i < 10; i++) {
                if ($scope.allInputsFilled[i] === true) {
                    validFlag = true;
                } else {
                    validFlag = false;
                    break;
                }
            }
            if (validFlag && $scope.legitUsername && $scope.legitPassword && $scope.legitSalutation && $scope.legitName && $scope.legitLastName && $scope.legitAddress1 && $scope.legitAddress2 && $scope.legitEmail && $scope.legitPhone && $scope.legitRole) {
                $('#createadmin').removeAttr("disabled");
            } else {
                $('#createadmin').attr("disabled", "true");
            }
        }

    }]);
