'use strict';

angular.module("eightMinutes")
  .config(["$stateProvider",function($stateProvider) {
  	$stateProvider
  		.state("admin.subAdmin", {
  			url : "/subAdmin",
  			templateUrl : "./pages/admin_subadmin/admin_subadmin.html",
 			controller : "subAdminController"
  		})
  }])
