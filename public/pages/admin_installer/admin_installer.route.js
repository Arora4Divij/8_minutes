'use strict';

angular.module("eightMinutes")
  .config(["$stateProvider",function($stateProvider) {
  	$stateProvider
  		.state("admin.installer", {
  			url : "/installer",
  			templateUrl : "./pages/admin_installer/admin_installer.html",
 			controller : "adminInstallerController"
  		})
  }])
