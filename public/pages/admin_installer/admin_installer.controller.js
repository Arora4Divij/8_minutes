angular.module('eightMinutes')
	.controller('adminInstallerController',["$scope","$state",function($scope,$state){

		var id = null; 
	var List;

	$("#addInstaller").on("click",function () {
		$('#myModal1').modal("show");
		$("#remove").hide()
		$("#createInst").html("Create")
		id = null; 
	})
	$("#cancel").on("click",function () {
		$('#myModal1').modal("hide");
	})

	function showModel(id) {
		$('#myModal1').modal("show");
		$("#remove").show()
		$("#createInst").html("Update")
		
		var item = List.filter(function (a) {
			return a._id == id;
		})[0] || {};

		$("#newInst-location").val(item.location);
		$("#newInst-desc").val(item.desc);
		$("#newInst-desig").val(item.desig);
		$("#newInst-phone").val(item.phone);
		$("#newInst-email").val(item.email);
		$("#newInst-name").val(item.name);
		$("#newInts-company").val(item.company_id);

		var $sel = $("#newInts-company");
		$sel.find("option").each(function () {
			$(this).removeAttr("selected")
			if($(this).attr("value") == item.company_id){
				$(this).attr("selected", "selected")							
			}
		})

		$(".select2").select2();

	}
	


	$("#createInst").on("click", function () {
		var data = {
						location   : $("#newInst-location").val(),
						desc       : $("#newInst-desc").val(),
						desig      : $("#newInst-desig").val(),
						phone      : $("#newInst-phone").val(),
						email      : $("#newInst-email").val(),
						name       : $("#newInst-name").val(),
						company_id : $("#newInts-company").val(),
						token      : window.sessionStorage.token
					};

		if(id){
			data ["_id"] = id;
		}

		$.ajax({
			url:"/api/installer" + (id ? ("/" + id) : "" ),
			type: id ? "PATCH" : "POST",
			data:data,
	        error: function () {
		        //document.location.href = "admin_ordatader-list.html"
	        },
			success: function (list) {
				document.location.href = "admin_installer-list.html"
			}
		});
	});


	$("#remove").on("click", function () {
		$.ajax({
			url:"/api/installer/" + id + "/delete" ,
			type: "POST",
			data:{token : window.sessionStorage.token},
	        error: function () {
		        //document.location.href = "admin_ordatader-list.html"
	        },
			success: function (list) {
				window.location.reload()
			}
		});
	});



	$.ajax({
		url:"/api/company/all",
		type:"POST",
		data:{token:window.sessionStorage.token},
        error: function () {
	        //document.location.href = "admin_order-list.html"
        },
		success: function (list) {
			$("#newInts-company").html("<option value=''>select</option>")
			for(var i in list){
				var item = list[i];
				var $elm = $("<option value=\""+ item._id + "\">"+ item.name +"</option>");
				$("#newInts-company").append($elm)
			}
			$(".select2").select2();
		}
	});


	$.ajax({
		url:"/api/installer/all",
		type:"POST",
		data:{token:window.sessionStorage.token},
        error: function () {
	        //document.location.href = "admin_order-list.html"
        },
		success: function (list) {
			List = list;
			for(var i in list){
				var item = list[i];
				var $elm = $([
					"<tr>",
						"<td><a href=\"javascript:void(0)\" class=\"edit\" alt=\""+ item._id +"\" >" + item.name + "</a></td>",
						"<td>" + (item.companyname || "") + "</td>",
						"<td>" + item.location + "</td>",
						"<td>" + item.phone + "</td>",
						"<td>" + item.email + "</td>",
					"</tr>"
				].join(''));
				$("#list").append($elm);
			}

			$(".edit").on("click",function () {
				id = $(this).attr("alt"); 
				showModel(id)
			})
		}
	});

		
	}]);