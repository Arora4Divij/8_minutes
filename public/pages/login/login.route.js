'use strict';

angular.module("eightMinutes")
  .config(["$stateProvider",function($stateProvider) {
  	$stateProvider
  		.state("login", {
  			url : "/login",
  			templateUrl : "./pages/login/login.html",
 			controller : "loginController"
  		})
  }])
