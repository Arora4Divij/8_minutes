angular.module('eightMinutes')
	.controller('loginController',["$scope","$state",function($scope,$state){
			


		$("#myModal").modal("show");

$("#loginForm").on("submit", function() {

		var loginUsername       = $("#loginUsername").val();
		var loginPassword     = $("#loginPassword").val();

		var data = {
			
			username : $("#userName").val(),
			password : $("#password").val()
		}
		

		$.ajax({
			url:"/api/login",
			type:"POST",
			data:data,
			error:function(d){
				$("#loginForm")[0].reset()
				showNotification("Error, Please enter valid credentials","error")
			},
			success: function (d) {
				if(d && d.success){
					$("#loginForm")[0].reset()
					window.sessionStorage.token = d.token;
					if(d.role && d.role === "admin"){
						// document.location.href = "admin_order-list.html"
						$("#myModal").modal("hide");
						$state.go('admin.orders');
					}else if(d.role === "user"){
						$("#myModal").modal("hide");
						$state.go('user.dashboard');
						// document.location.href = "user_dashboard.html"
					}else if(d.role === "sub-admin"){
						document.location.href = "sub_admin_dashboard.html"
					}

				}	
			}
		})
});

$(".forgotUserName").on("click",function () {
	document.getElementById('login').style.display = 'none'; 
	document.getElementById('forgotUserNamePasswordSec').style.display = 'block'; 
	document.getElementById('register').style.display = 'none';
})

$(".loginBtn").on("click",function () {
	document.getElementById('login').style.display = 'block'; 
	document.getElementById('forgotUserNamePasswordSec').style.display = 'none'; 
	document.getElementById('register').style.display = 'none';
});

$(".registerBtn").on("click", function () {
	document.getElementById('login').style.display = 'none'; 
	document.getElementById('forgotUserNamePasswordSec').style.display = 'none'; 
	document.getElementById('register').style.display = 'block';
});

$("#loginlnk").on("click",function(){
	document.getElementById('login').style.display = 'block'; 
	document.getElementById('forgotUserNamePasswordSec').style.display = 'none'; 
	document.getElementById('register').style.display = 'none';
	$("#myModal").modal("show")

})


$("#CreateAcc").on("submit", function() {


		var data = {
			"username": $("#loginUsername").val(),
			"password": $("#reg-password").val(),
			"salutation":$("#salutation").val(),
			"fname":$("#fname").val(),
			"lname":$("#lname").val(),
			"address1":$("#address1").val(),
			"address2":$("#address2").val(),
			"address3":$("#address3").val(),
			"email":$("#userEmail").val(),
			"pin":$("#pin").val(),
			"phone":$("#phone").val()
		}


		$.ajax({
			url:"/api/register",
			type:"POST",
			data:data,
			error:function(d){
				showNotification("Error! Please try with other username" ,"error")
			},
			success: function (d) {
				$("#loginForm")[0].reset()
				showNotification("Success! Your account is created, please login to continue","success")
				document.getElementById('login').style.display = 'block'; 
				document.getElementById('forgotUserNamePasswordSec').style.display = 'none'; 
				document.getElementById('register').style.display = 'none';
			}
		})
});
$("#forgotCred").on("submit", function() {

		var data = {};
		var type = $('[name="forgotRadio"]:checked').val();
		data["email"] = $('#forgot-email').val();

		$.ajax({
			url:"/api/forgot-" + type,
			type:"POST",
			data:data,
			success: function (d) {
				$("#loginForm")[0].reset()
				$("#forgotCred")[0].reset()
				showNotification("Success, Please watch out your mail for further details","success")
				document.getElementById('login').style.display = 'block'; 
				document.getElementById('forgotUserNamePasswordSec').style.display = 'none'; 
				document.getElementById('register').style.display = 'none';
			}
		})
});

$(".select2").select2();
	

	}]);

