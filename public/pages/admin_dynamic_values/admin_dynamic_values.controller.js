angular.module('eightMinutes')
	.controller('dynamicValuesController',["$scope","$state","CommonService",function($scope,$state,CommonService){
			
			$scope.editVal = false;
			$scope.stateIndex = "na";
			$scope.showStateInfo = false;


			CommonService.requestApi('POST','/api/dynamic_values/all',"","",{}).then(function(data){
				console.log(data.data[0]);
				$scope.dVal = data.data[0];
				$scope.eVal = JSON.parse(JSON.stringify($scope.dVal));
				console.log($scope.dVal.state[0]);
			});


			

			$scope.showEditForm = function(){
				$scope.editVal = true;
			}

			$scope.submitNewVal = function(){
				// console.log($scope.eVal);
				body = {"newData": $scope.eVal}
				CommonService.requestApi('POST','/api/dynamic_val/updateOther',"","", body).then(function(data){
					console.log(data);
					$scope.dVal = data.data[0];
					$scope.eVal = JSON.parse(JSON.stringify($scope.dVal));
					
				});
			}

			$scope.hideEditFormVal = function(){
				$scope.editVal = false;
			}

			$scope.displayState = function(){
				$scope.stateDetails = $scope.dVal.state[$scope.stateIndex];
				$scope.showStateInfo = true;
			}
	}]);