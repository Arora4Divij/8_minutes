'use strict';

angular.module("eightMinutes")
  .config(["$stateProvider",function($stateProvider) {
  	$stateProvider
  		.state("admin.dynamicVal", {
  			url : "/dynamic_values",
  			templateUrl : "./pages/admin_dynamic_values/admin_dynamic_values.html",
 			controller : "dynamicValuesController"
  		})
  }])
