'use strict';

angular.module("eightMinutes")
  .config(["$stateProvider",function($stateProvider) {
  	$stateProvider
  		.state("admin.companies", {
  			url : "/companies",
  			templateUrl : "./pages/admin_companies/admin_companies.html",
 			controller : "adminCompaniesController"
  		})
  }])
