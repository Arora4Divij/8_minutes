angular.module('eightMinutes')
	.controller('adminCompaniesController',["$scope","$state",function($scope,$state){


			var curInstId = null; 
	var instList;

	$("#addInstaller").on("click",function () {
		$('#myModal1').modal("show");
		$("#remove").hide()
		$("#createInst").html("Create")
		curInstId = null; 
	})

	$("#cancel").on("click",function () {
		$('#myModal1').modal("hide");
	})

	function showModel(id) {
		$('#myModal1').modal("show");
		$("#remove").show();
		$("#createInst").html("Update");
		
		var item = instList.filter(function (a) {
			return a._id == id;
		})[0] || {};

		$("#newComp-name").val(item.name);
		$("#newComp-address").val(item.address);
		$("#newComp-type").val(item.type);
		$("#newComp-email").val(item.email);
		$("#newComp-phone").val(item.phone);
		$("#newComp-financial_turnover").val(item.financial_turnover);
		$("#newComp-team_strength").val(item.team_strength);
		$("#newComp-total_completed_projects").val(item.total_completed_projects);
		$("#newComp-team_experience").val(item.team_experience);
		$("#newComp-facilities").val(item.facilities);
		$("#newComp-cities").val(item.cities);
		$("#newComp-bank_name").val(item.bank_name);
		$("#newComp-branch_name").val(item.branch_name);

		var $sel = $("#newInts-company");
		$sel.find("option").each(function () {
			$(this).removeAttr("selected")
			if($(this).attr("value") == item.company_id){
				$(this).attr("selected", "selected")							
			}
		})

		$(".select2").select2();

	}
	


	$("#createInst").on("click", function () {
		var data = {
						name                     : $("#newComp-name").val(),
						address                  : $("#newComp-address").val(),
						type                     : $("#newComp-type").val(),
						email                    : $("#newComp-email").val(),
						phone                    : $("#newComp-phone").val(),
						financial_turnover       : $("#newComp-financial_turnover").val(),
						team_strength            : $("#newComp-team_strength").val(),
						total_completed_projects : $("#newComp-total_completed_projects").val(),
						team_experience          : $("#newComp-team_experience").val(),
						facilities               : $("#newComp-facilities").val(),
						cities                   : $("#newComp-cities").val(),
						bank_name                : $("#newComp-bank_name").val(),
						branch_name              : $("#newComp-branch_name").val(),

						token      : window.sessionStorage.token
					};

		if(curInstId){
			data ["_id"] = curInstId;
		}

		$.ajax({
			url:"/api/company" + (curInstId ? ("/" + curInstId) : "" ),
			type: curInstId ? "PATCH" : "POST",
			data:data,
	        error: function () {
		        //document.location.href = "admin_ordatader-list.html"
	        },
			success: function (list) {
				document.location.href = "admin_companies-list.html"
			}
		});
	});


	$("#remove").on("click", function () {
		$.ajax({
			url:"/api/company/" + curInstId + "/delete" ,
			type: "POST",
			data:{token : window.sessionStorage.token},
	        error: function () {
		        //document.location.href = "admin_ordatader-list.html"
	        },
			success: function (list) {
				document.location.href = "admin_companies-list.html"
			}
		});
	});

	$("#edit").on("click", function () {
		curInstId =  $("#companyList").val();
		showModel(curInstId)

	});
	$("#show").on("click", function () {

		$(".companyDetails").show();

		curInstId =  $("#companyList").val();
		var item = instList.filter(function (a) {
			return a._id == curInstId;
		})[0] || {};

		$("#name").html(item.name);
		$("#address").html(item.address);
		$("#type").html(item.type);
		$("#email").html(item.email);
		$("#phone").html(item.phone);
		$("#financial_turnover").html(item.financial_turnover);
		$("#team_strength").html(item.team_strength);
		$("#total_completed_projects").html(item.total_completed_projects);
		$("#team_experience").html(item.team_experience);
		$("#facilities").html(item.facilities);
		$("#cities").html(item.cities);
		$("#bank_name").html(item.bank_name);
		$("#branch_name").html(item.branch_name);

	})


	$.ajax({
		url:"/api/company/all",
		type:"POST",
		data:{token:window.sessionStorage.token},
        error: function () {
	        //document.location.href = "admin_order-list.html"
        },
		success: function (list) {
			instList = list;
			for(var i in list){
				var item = list[i];
				var $elm = $("<option value=\"" + item._id + "\">" + item.name + "</option>");
				$("#companyList").append($elm);
			}

			$(".select").select2()
		}
	});


	}]);