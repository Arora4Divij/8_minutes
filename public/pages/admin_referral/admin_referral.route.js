'use strict';

angular.module("eightMinutes")
  .config(["$stateProvider",function($stateProvider) {
  	$stateProvider
  		.state("admin.referrals", {
  			url : "/referrals",
  			templateUrl : "./pages/admin_referral/admin_referral.html",
 			controller : "adminReferralsController"
  		})
  }])
