angular.module('eightMinutes')
	.controller('adminReferralsController',["$scope","$state",function($scope,$state){
		var id = null; 
	var list;

	$("#addInstaller").on("click",function () {
		$('#myModal1').modal("show");
		$("#remove").hide()
		$("#createInst").html("Create")
		id = null; 
	})
	$("#cancel").on("click",function () {
		$('#myModal1').modal("hide");
	})

	function showModel(id) {
		$('#myModal1').modal("show");
		$("#remove").show()
		$("#createInst").html("Update")
		
		var item = list.filter(function (a) {
			return a._id == id;
		})[0] || {};

		$("#rewards").val(item.rewards);

		var $sel = $("#status");
		$sel.find("option").each(function () {
			$(this).removeAttr("selected")
			if($(this).attr("value") == item.status){
				$(this).attr("selected", "selected")							
			}
		})

		$(".select2").select2();

	}
	


	$("#createInst").on("click", function () {
		var data = {
						status  : $("#status").val(),
						rewards : $("#rewards").val(),
						token   : window.sessionStorage.token
					};

		if(id){
			data ["_id"] = id;
		}

		$.ajax({
			url:"/api/referral" + (id ? ("/" + id) : "" ),
			type: id ? "PATCH" : "POST",
			data:data,
	        error: function () {
		        //document.location.href = "admin_ordatader-list.html"
	        },
			success: function (list) {
				window.location.reload()
			}
		});
	});


	$("#remove").on("click", function () {
		$.ajax({
			url:"/api/referral/" + id + "/delete" ,
			type: "POST",
			data:{token : window.sessionStorage.token},
	        error: function () {
		        //document.location.href = "admin_ordatader-list.html"
	        },
			success: function (list) {
				window.location.reload()
			}
		});
	});



	$.ajax({
		url:"/api/referral/all",
		type:"POST",
		data:{token:window.sessionStorage.token},
        error: function () {
	        //document.location.href = "admin_order-list.html"
        },
		success: function (list) {
			list = list;
			for(var i in list){
				var item = list[i];
				var $elm = $([
					"<tr>",
						"<td><a href=\"javascript:void(0)\" class=\"edit\" alt=\""+ item._id +"\" >" + (item.salutation||"") +" " + item.fname + " " + item.fname + "</a></td>",
						"<td>" + item.created_on + "</td>",
						"<td>" + item.phone + "</td>",
						"<td>" + item.email + "</td>",
						"<td>" + item.address + "</td>",
						"<td>" + item.username + "</td>",
						"<td>" + item.status + "</td>",
						"<td><i class='fa fa-inr valLabel'></i>" + item.rewards + "</td>",
					"</tr>"
				].join(''));
				$("#list").append($elm);
			}

			$(".edit").on("click",function () {
				id = $(this).attr("alt"); 
				showModel(id)
			})
		}
	});

	}]);