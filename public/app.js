app = angular.module('eightMinutes',["ui.router","angular-storage"])
	.config(["$urlRouterProvider","storeProvider","$httpProvider","$locationProvider",function($urlRouterProvider,storeProvider,$httpProvider,$locationProvider) {
    $urlRouterProvider
    .otherwise('/login');

    storeProvider.setStore('localStorage') //can be changed into sessionStorege or cookieStorage
    $locationProvider.html5Mode(true);
    // $httpProvider.interceptors.push('myInterceptor');
  }])