var Schema = require('../db-models/sub_admin_map');
var util = require('../util');
var User  = require('../db-models/user');
var Controller = {
    add : function (req, res) {
        
        var newRecord = new Schema(newEnty);
        newRecord.save(function(e,record) {
            return util.apiResp(req,res,e,record)
        });
        
    },
    getById: function(req, res) {
        Schema.findOne({username: req.decoded._doc.username}, function(e, record) {
            return util.apiResp(req,res,e,record)
        });
    },
    getAll: function(req, res) {
        if(req.decoded._doc.role === "admin"){
            var resData = {};
            Schema.find({}, function(e, recordsInSubAdmin) {
                    User.find({"role":"sub-admin"}, function(e, recordsInUser){
                        resData.user = recordsInUser;
                        resData.subAdmin = recordsInSubAdmin;
                        res.send(resData);
                    });
            });
        }
        else{
            res.send({"message":"Not Authorized"});
        }
    },
    putById: function (req, res) {
        Schema.findOneAndUpdate({ _id: req.params.id }, {$set:util.rmExtToUpdate(Schema,req)}, {new: true}, function(e, record){
            return util.apiResp(req,res,e,record)
        });
    },
    deleteById: function (req, res) {
        //req.params.id
        // body...
        Schema.findOneAndRemove({ _id: req.params.id }, function(e, record){
            return util.apiResp(req,res,e,record)
        });
    }
}
module.exports = Controller;