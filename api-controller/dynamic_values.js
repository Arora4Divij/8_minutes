var Schema = require('../db-models/dynamic_values');
var util = require('../util');
var Controller = {
	add : function (req, res) {

				if(req.decoded._doc.role === "admin"){
					console.log("request came", req.body);
		            var newRecord = new Schema(req.body.data);
		            newRecord.save(function(e,record) {
		                return util.apiResp(req,res,e,record)
            			});
		        }
		        else{
		        	res.send({"message": "Not Authorised"});
		        }
   		

	},
	getById: function(req, res) {
		//
		// Schema.findOne({ _id: req.params.id }, function(e, record) {
	 //        return util.apiResp(req,res,e,record)
	 //    });
	},
    getAll: function(req, res) {
        Schema.find({}, function(e, records) {
            return util.apiResp(req,res,e,records)
        });
    },
	putById: function (req, res) {
		// Schema.findOneAndUpdate({ _id: req.params.id }, {$set:util.rmExtToUpdate(Schema,req)}, {new: true}, function(e, record){
		//     return util.apiResp(req,res,e,record)
		// });
	},
	deleteById: function (req, res) {
		// Schema.findOneAndRemove({ _id: req.params.id }, function(e, record){
		//           return util.apiResp(req,res,e,record)
		//       });
	},
	insertState: function(req,res){
		if(req.decoded._doc.role === "admin"){
		Schema.findOne({},function(err,data){
			if(data._id){
				Schema.findByIdAndUpdate(
				    data._id,
				    {$push: {"state" : req.body.data}},
				    {safe: true, upsert: true},
				    function(err, model) {
				        console.log(err);
				        if(err){
				        	res.send({"error":"error in query please check request object"});
				        }
				        if(!err){
				        	res.send({"msg":"OK"})
				        }
				    }
				);
			}
			else
				res.send({"message": "database is empty"});
		});
		}
		else{
			res.send({"message": "Not Authorised"});
		}
	},
	updateState: function(req,res){
		if(req.decoded._doc.role === "admin"){
		Schema.update({'state.name': req.body.data.oldName}, {'$set': {
		    'state.$.name': req.body.data.newName,
		    'state.$.generation_sum': req.body.data.generation_sum,
		    'state.$.distribution' : req.body.data.distribution
		}}, function(err,data) {
			if(err){
				res.send({"message":"unable to update"});
			}
			else{
				res.send({"message":"done"});
			}
		});
		}
		else{
			res.send({"message": "Not Authorised"});
		}
	},
	updateOther: function(req,res){
		if(req.decoded._doc.role === "admin"){
		Schema.findOne({},function(err,data){
			if(data._id){


				Schema.findOneAndUpdate(data._id, req.body.newData, {upsert:false}, function(err, doc){
				    if (err) {
				    	res.send(500, { error: err });
				    }
				    else{
				    	Schema.find({}, function(e, records) {
				            return util.apiResp(req,res,e,records)
				        });
				    } 
				});
			}
			else{
				res.send({"message": "data not found"});
			}
		});
		}
		else{
			res.send({"message": "Not Authorised"});
		}
	}
}
module.exports = Controller;